//
// Created by marco on 2/7/19.
//

#ifndef SPACEWALK_BLACKHOLE_H
#define SPACEWALK_BLACKHOLE_H


#include "Drawable.h"
#include "SpaceObject.h"

class BlackHole : public SpaceObject{
public:
    BlackHole(int _y, int _x, int _id);
    void draw() override;

};


#endif //SPACEWALK_BLACKHOLE_H
