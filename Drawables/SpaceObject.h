//
// Created by marco on 2/12/19.
//

#ifndef SPACEWALK_SPACEOBJECT_H
#define SPACEWALK_SPACEOBJECT_H


#include "Drawable.h"

class SpaceObject : public Drawable {
public:
    int id;
    bool isPlanet;
};


#endif //SPACEWALK_SPACEOBJECT_H
