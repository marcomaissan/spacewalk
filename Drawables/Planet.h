//
// Created by marco on 2/7/19.
//

#ifndef SPACEWALK_PLANET_H
#define SPACEWALK_PLANET_H


#include <vector>
#include "Drawable.h"
#include "SpaceObject.h"

class Planet : public SpaceObject{
public:
    Planet(int _y, int _x, int _id);
    ~Planet();
    void draw() override;
};


#endif //SPACEWALK_PLANET_H
