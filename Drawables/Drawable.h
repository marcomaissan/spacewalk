//
// Created by marco on 2/7/19.
//

#ifndef SPACEWALK_DRAWABLE_H
#define SPACEWALK_DRAWABLE_H


class Drawable {
public:
    virtual void draw() = 0;
    int x, y;
    int cursorPosition;
};


#endif //SPACEWALK_DRAWABLE_H
