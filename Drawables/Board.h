//
// Created by marco on 2/9/19.
//

#ifndef SPACEWALK_BOARD_H
#define SPACEWALK_BOARD_H


#include "Drawable.h"

class Board : public Drawable{
public:
    Board();
    void  draw() override;
private:
    int x, y;
    void generateBorders();
};


#endif //SPACEWALK_BOARD_H
