//
// Created by marco on 2/9/19.
//

#include <ncurses.h>
#include "Board.h"


Board::Board() {

}

void Board::generateBorders(){
    //Get xy dimensions
    getmaxyx(stdscr, y, x);
    //draw horizontal lines
    move(0,0);
    hline('-', x-1);
    move((int)(y*.75), 0);
    hline('-', x-1);

    //draw vertical lines
    move(1, 0);
    vline('|', (int)(y*.75)-1);
    move(1, x-1);
    vline('|',(int)(y*.75)-1);
}

void Board::draw() {
    clear();
    generateBorders();
}



