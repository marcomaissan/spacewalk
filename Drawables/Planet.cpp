//
// Created by marco on 2/7/19.
//

#include <iostream>
#include <ncurses.h>
#include <array>
#include <charconv>
#include "Planet.h"

void Planet::draw() {
    move(y, x);
    //id to character conversion
    std::string s = std::to_string(id);
    char const *pchar = s.c_str();

    move(y, x);
    hline('=', 12);
    printw(pchar);
    move(y + 1, x);
    vline('|', 7);
    move(y + 8, x);
    hline('=', 12);
    move(y + 1, x + 11);
    vline('|', 7);
}

Planet::Planet(int _y, int _x, int _id) {
    x = _x;
    y = _y;
    id = _id;
    isPlanet = true;
    cursorPosition = 0;
}

Planet::~Planet() {
    std::cout << "Planet destructor called" << std::endl;
    //Destructor
}
