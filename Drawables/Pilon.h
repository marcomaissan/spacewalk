//
// Created by marco on 2/11/19.
//

#ifndef SPACEWALK_PILON_H
#define SPACEWALK_PILON_H


#include "Drawable.h"
#include "../GameLogic/Player.h"
#include "Planet.h"


class Pilon : public Drawable{
public:
    Pilon(char t, SpaceObject *p, Player *pl);
    void draw() override;
    SpaceObject *planet;
    char pilonType;
    bool isBlocked;
private:
    Player *player;
};


#endif //SPACEWALK_PILON_H
