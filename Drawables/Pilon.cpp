//
// Created by marco on 2/11/19.
//

#include <ncurses.h>
#include "Pilon.h"
#include "Planet.h"

void Pilon::draw() {
    int x = planet->x;
    int y = planet->y;
    move(y+1+planet->cursorPosition, x+1);

    start_color();			/* Start color 			*/
    init_pair(player->id+1, static_cast<short>(player->color), COLOR_BLACK);

    attron(COLOR_PAIR(player->id+1));
    addch(pilonType);
    attroff(COLOR_PAIR(player->id+1));

//    getch();
    planet->cursorPosition++;

}

Pilon::Pilon(char t, SpaceObject *p, Player *pl) {
    this->pilonType = t;
    this->planet = p;
    this->player = pl;
    this->cursorPosition = 0;
}


