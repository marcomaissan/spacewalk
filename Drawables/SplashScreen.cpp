//
// Created by marco on 2/7/19.
//
#include <ncurses.h>
#include <cstring>
#include "SplashScreen.h"


//Static method, has no fuctionality other than showing text
void SplashScreen::showSplash() {
    char mesg[]="S P A C E    W A L K";		/* message to be appeared on the screen */
    char subtitle[]="By Marco Maissan";
    char next[]="Press any key to continue...";
    int row,col;
    getmaxyx(stdscr,row,col);
    noecho();
    attron(A_BOLD);
    attron(A_UNDERLINE);
    mvprintw(row/2,(int)(col-strlen(mesg))/2,"%s",mesg);
    attroff(A_UNDERLINE);
    mvprintw((row/2)+2, (int)(col-strlen(subtitle))/2, "%s", subtitle);
    attroff(A_BOLD);
    mvprintw((row/2)+3, (int)(col-strlen(next))/2, "%s", next);
    /* print the message at the center of the screen */
    getch();
}
