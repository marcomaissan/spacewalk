#include <iostream>
#include <ncurses.h>
#include <string.h>
#include "GameLogic/Game.h"
#include "Drawables/Planet.h"

#include <vector>

int main()
{
    //Welcome to Space Walk!
    //Allocate game on stack, on dynamic allocation required
    Game game;
    game.play();

    return 0;
}