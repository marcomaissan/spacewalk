//
// Created by marco on 2/11/19.
//

#include <ncurses.h>
#include <sstream>
#include <cstring>
#include "Evacuator.h"
#include "../Drawables/SpaceObject.h"
#include "../Drawables/Pilon.h"

void
Evacuator::evacuate(Player *player, std::vector<SpaceObject *> *planets, std::vector<Pilon *> *pilons) {
    int planetToEvacuate = choosePlanet(player);

    if(planetToEvacuate == 0)return;

    if(!planets->at(planetToEvacuate-1)->isPlanet){
        int x, y;
        getmaxyx(stdscr, y, x);
        move((y * .75) + 2, 0);
        printw("You cannot evacuate a Black Hole!!!");
        getch();
    }

    std::vector<Pilon *> pilonsToEvacuate;

    /*
     * Generate list of pilons to
     */
    for (int i = 0; i < pilons->size(); i++) {
        //Get a pilon
        Pilon *pilon = pilons->at(i);
        //If pilon's planet id equals evacuatable planet
        if (pilon->planet->id == planetToEvacuate) {
            pilonsToEvacuate.push_back(pilon);
        }
    }

    /*
     * Loop through pilons to evacuate and replace them to different planet
     */
    int counter = 0;
    for (int j = 0; j < pilonsToEvacuate.size(); j++) {
        if (pilonsToEvacuate.at(j)->pilonType == 'L' && !pilonsToEvacuate.at(j)->isBlocked) {
            pilonsToEvacuate.at(j)->planet = planets->at((planetToEvacuate + counter) % 14);
            counter++;
        }
    }
    for (int j = 0; j < pilonsToEvacuate.size(); j++) {
        if (pilonsToEvacuate.at(j)->pilonType == 'M' && !pilonsToEvacuate.at(j)->isBlocked) {
            pilonsToEvacuate.at(j)->planet = planets->at((planetToEvacuate + counter) % 14);
            counter++;
        }
    }
    for (int j = 0; j < pilonsToEvacuate.size(); j++) {
        if (pilonsToEvacuate.at(j)->pilonType == 'S' && !pilonsToEvacuate.at(j)->isBlocked) {
            pilonsToEvacuate.at(j)->planet = planets->at((planetToEvacuate + counter) % 14);
            counter++;
        }
    }


    for(int i = 0; i < pilons->size(); i++){
        if(!pilons->at(i)->planet->isPlanet){
            pilons->at(i)->isBlocked = true;
        }
    }
}



int Evacuator::choosePlanet(Player* player) {
    int x, y;
    char mesg[3];
    char mesg2[3];

    getmaxyx(stdscr, y, x);
    move((y * .75) + 1, 0);
    echo();
    printw("Planet to evacuate by ");
    char name[10];
    player->getName(name);
    printw(name);
    printw(": ");

    //Choose computer or person
    if(strcmp(name, "Computer") == 0){
        int random = rand() % 14 + 1;
        sprintf(mesg,"%ld", random);
    }else{
        getstr(mesg);
    }

    move((y * .75) + 2, 0);
    printw("Evacuated planet number ");
    std::ostringstream oss;
    oss << mesg;
    std::string var = oss.str();
    strcpy(mesg2, var.c_str());
    printw(mesg2);
    int i = atoi(mesg);
    getch();
    return (i);
}

