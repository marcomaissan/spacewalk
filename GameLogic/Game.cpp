//
// Created by marco on 2/7/19.
//

#include <ncurses.h>
#include "Game.h"
#include "GameInitializer.h"
#include "../Drawables/SplashScreen.h"
#include "../Drawables/Board.h"
#include "GameFactory.h"
#include "Evacuator.h"
#include "../Drawables/SpaceObject.h"

void Game::play() {
    //start ncurses
    initscr();

    //set color
    start_color();            /* Start color 			*/
    init_pair(0, COLOR_WHITE, COLOR_BLACK);

    attron(COLOR_PAIR(0));


    //Make initializer pointer, containing all initialisation information of game
    //E.g. board info, amount of players, etc.
    auto *initializer = new GameInitializer();

    //Show splash screen
    initializer->showSplashScreen();

    //initialize the game
    initializer->initializeGame();

//    delete initializer;

    GameFactory f{};
    std::vector<SpaceObject *> boardItems = f.boardItems();

    Board board;
    board.draw();

    for (auto y : boardItems) {
        y->draw();
    }

    getch();

    std::vector<Pilon *> pilons = f.createPilons(&initializer->players);
    for (auto z : pilons) {
        z->draw();
    }

    int moduloCounter = 0;

    //Start actual game loop
    Evacuator evacuator;
    evacuator.evacuate(initializer->players.at(moduloCounter % initializer->players.size()), &boardItems, &pilons);

    while(true){

        clear();
        for(int i = 0; i < boardItems.size(); i++){
            SpaceObject *x = boardItems.at(i);
            SpaceObject *y = x;
        }

        for (SpaceObject* bla : boardItems) {
            bla->cursorPosition = 0;
        }
        board.draw();
        for (auto y : boardItems) {
            y->draw();
        }
        for (auto z : pilons) {
            z->draw();
        }
        moduloCounter++;

        evacuator.evacuate(initializer->players.at(moduloCounter % initializer->players.size()), &boardItems, &pilons);
    }
    getch();

    //end ncurses
    endwin();
}
