//
// Created by marco on 2/7/19.
//

#include "GameInitializer.h"
#include "../Drawables/SplashScreen.h"
#include "Initializer.h"

Initializer *initializer;

GameInitializer::GameInitializer() {
    //Constructor
    //Allocate new initializer object on heap
    initializer = new Initializer();
}

GameInitializer::~GameInitializer() {
    delete initializer;
}

void GameInitializer::initializeGame() {
    //give reference to list of players
    initializer->generatePlayers(players);

    initializer->setPlayerSettings(players);
}

void GameInitializer::showSplashScreen() {
    //Static call, no functionality other than showing stuff on screen
    SplashScreen::showSplash();
}




