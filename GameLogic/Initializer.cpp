//
// Created by marco on 2/8/19.
//

#include "Initializer.h"
#include "Player.h"
#include <ncurses.h>
#include <cstring>
#include <sstream>
#include <menu.h>
#include <iostream>

void Initializer::generatePlayers(std::vector<Player *> &vector) {
    clear();
    char mesg[] = "Enter amount of players: ";
    char mesg2[500];
    int row, col;

    echo();
    getmaxyx(stdscr, row, col);
    mvprintw(row / 2, (int) ((col - strlen(mesg)) / 2), "%s", mesg);
    players = getch() - '0';

    for (int i = 0; i < players; i++) {
        vector.push_back(new Player(i));
    }

    //make string stream
    std::ostringstream oss;
    oss << "Playing with " << players << " players. Press any key to continue...";
    std::string var = oss.str();
    strcpy(mesg2, var.c_str());

    mvprintw((row / 2) + 1, (int) ((col - strlen(mesg2)) / 2), "%s", mesg2);
    getch();
}

void Initializer::setPlayerSettings(std::vector<Player *> &players) {
    for (int i = 0; i < players.size(); i++) {
        clear();
        char buffer[200];
        char ans[10];
        int row, col;

        echo();

        std::ostringstream oss;
        oss << "Player " << (i + 1) << " name: ";
        std::string var = oss.str();
        strcpy(buffer, var.c_str());

        getmaxyx(stdscr, row, col);
        mvprintw(row / 2, (int) ((col - strlen(buffer)) / 2), "%s", buffer);
        getstr(ans);

        players[i]->setName(ans);

        //make string stream
        oss.str("");
        oss.clear();

        //Placing names in ans and showing name
        players[i]->getName(ans);

        oss << ans << "'s color will be " << colors[i+1];
        players[i]->setColor(i+1);

        var = oss.str();
        strcpy(buffer, var.c_str());
        mvprintw((row / 2) + 1, (int) ((col - strlen(buffer)) / 2), "%s", buffer);

        getch();
    }

//    for (int i = 0; i < players.size(); i++) {
//        endwin();
//        char swag[10];
//        players[i]->getName(swag);
//        std::cout << swag << std::endl;
//    }
}

Initializer::~Initializer() {
    //empty
}


