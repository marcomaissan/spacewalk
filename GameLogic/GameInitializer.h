//
// Created by marco on 2/7/19.
//

#ifndef SPACEWALK_GAMEINITIALIZER_H
#define SPACEWALK_GAMEINITIALIZER_H


#include <vector>
#include "Player.h"

class GameInitializer {
public:
    GameInitializer();
    ~GameInitializer();
    void initializeGame();
    static void showSplashScreen();
    std::vector<Player*> players;
};


#endif //SPACEWALK_GAMEINITIALIZER_H
