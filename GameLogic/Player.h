//
// Created by marco on 2/8/19.
//

#ifndef SPACEWALK_PLAYER_H
#define SPACEWALK_PLAYER_H
#define NAME_SIZE 10

class Player {
private:
    char name[NAME_SIZE];
public:
    Player(int id);
    void setName(char nameref[NAME_SIZE]);
    void getName(char *name);
    void setColor(int color);
    int color;
    int id;
};


#endif //SPACEWALK_PLAYER_H
