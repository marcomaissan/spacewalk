//
// Created by marco on 2/8/19.
//

#ifndef SPACEWALK_INITIALIZER_H
#define SPACEWALK_INITIALIZER_H


#include <vector>
#include "Player.h"
#include <string>

class Initializer {
public:

    ~Initializer();
    //Determine players
    void generatePlayers(std::vector <Player*> &vector);
    //Sets player settings like name and color
    void setPlayerSettings(std::vector <Player*> &vector);
    std::string colors[8] = {
        "black",
        "red",
        "green",
        "yellow",
        "blue",
        "magenta",
        "cyan",
        "white",
    };
    int players;
};


#endif //SPACEWALK_INITIALIZER_H
