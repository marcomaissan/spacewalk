//
// Created by marco on 2/11/19.
//

#ifndef SPACEWALK_EVACUATOR_H
#define SPACEWALK_EVACUATOR_H


#include "../Drawables/Drawable.h"
#include "Player.h"
#include "../Drawables/SpaceObject.h"
#include "../Drawables/Pilon.h"
#include <vector>

class Evacuator {
public:
    void evacuate(Player *players,  std::vector<SpaceObject *> *planets, std::vector<Pilon *> *pilons);
    int choosePlanet(Player* player);
private:
//    void Evacuator::movePilon(int &counter, char type, std::vector<Pilon *> pilonsToEvaluate);
};


#endif //SPACEWALK_EVACUATOR_H
