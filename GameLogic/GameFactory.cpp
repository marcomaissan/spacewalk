//
// Created by marco on 2/10/19.
//

#include <ncurses.h>
#include <cstdlib>
#include "GameFactory.h"
#include "../Drawables/Board.h"
#include "../Drawables/Planet.h"
#include "../Drawables/BlackHole.h"
#include "Player.h"
#include "../Drawables/Pilon.h"
#include "../Drawables/SpaceObject.h"

std::vector<SpaceObject *> GameFactory::boardItems() {
    /*
     *
     * Putting all the planets in place
     * Using size of display to determine where each object will be written.
     *
     */

//    v.push_back(new Board);
    int x, y;
    getmaxyx(stdscr, y, x);

    for (int i = 0; i < 6; i++) {
        /*
         * Make planet on first line
         * Divide amount of planets over width of screen, and add margin of 6 to the left
         */
        v.push_back(new Planet(1, ((x / 6) * i) + 10, i+1));
    }

    v.push_back(new BlackHole(11, x - 14, 7));

    for (int i = 0; i < 8; i++) {
        /*
         * Make planet on first line
         * Divide amount of planets over width of screen, and add margin of 6 to the left
         */
        if (i != 0 && i != 7)
            v.push_back(new Planet(14, x-((x / 8) * i) - 20, i+7));
    }

    v.push_back(new BlackHole(11, 3, 14));

    for (int i = 0; i < 11; i++) {
        /*
         * Make planet on last line
         * height * 0.75 minus the height of planet is y coordinate
         */
        if(i != 5){
        v.push_back(new Planet((int) ((y * .75) - 9), x-((x / 11) * i) - 16, i+16));
        }else{
            //put blackhole on xth position
            v.push_back(new BlackHole((int) ((y * .75) - 9), x-((x / 11) * i) - 16, i+16));
        }
    }
    v.push_back(new Planet(21, 3, 16));
    v.push_back(new Planet(21, x - 14, 15));
    return v;
}

std::vector<Pilon *> GameFactory::createPilons(std::vector<Player *> *players) {
    //Make an array with the possible numbers, so all planet numbers excluding black holes
    std::vector<int> choosableNrs = {1,2,3,4,5,6,8,9,10,11,12,13};

    //Loop for each player
    for(int i = 0; i < players->size(); i++){

        //Make for loop for 3x each type of planet and add all.
        for(int j = 0; j < 9; j++){
            int planetid = static_cast<int>(rand() % choosableNrs.size());
            SpaceObject *p = v[choosableNrs[planetid]-1];
            Player *pl = players->at(static_cast<unsigned long>(i));
            Pilon *x = nullptr;
            if(j > 5){
                 x =  new Pilon('S', p, pl);
            }else if(j > 2){
                 x =  new Pilon('M', p, pl);
            }else {
                 x =  new Pilon('L', p, pl);
            }
            this->p.push_back(x);
        }
    }

    return p;
}

GameFactory::~GameFactory() {

}
