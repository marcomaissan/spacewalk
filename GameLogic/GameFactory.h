//
// Created by marco on 2/10/19.
//

#ifndef SPACEWALK_GAMEFACTORY_H
#define SPACEWALK_GAMEFACTORY_H


#include <vector>
#include "../Drawables/Drawable.h"
#include "Player.h"
#include "../Drawables/SpaceObject.h"
#include "../Drawables/Pilon.h"

class GameFactory {
public:
    std::vector<SpaceObject*> boardItems();
    std::vector<Pilon*> createPilons(std::vector<Player *> *players);
    ~GameFactory();
private:
    std::vector<SpaceObject*> v;
    std::vector<Pilon*> p;
};


#endif //SPACEWALK_GAMEFACTORY_H
